// Fill out your copyright notice in the Description page of Project Settings.


#include "SnakeBonusSpeed.h"
#include "SnakeBase.h"
#include "Kismet/KismetSystemLibrary.h"

// Sets default values
ASnakeBonusSpeed::ASnakeBonusSpeed()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

}

// Called when the game starts or when spawned
void ASnakeBonusSpeed::BeginPlay()
{
	Super::BeginPlay();
	
}

// Called every frame
void ASnakeBonusSpeed::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

void ASnakeBonusSpeed::Interact(AActor* Interactor, bool bIsHead)
{
	if (bIsHead)
	{
		auto Snake = Cast<ASnakeBase>(Interactor);
		if (IsValid(Snake))
		{
			//���������� �������� ������
			if (Snake->GetActorTickInterval() >= 0.2)
			{
				Snake->SetActorTickInterval(Snake->GetActorTickInterval() - 0.1);
				//UKismetSystemLibrary::PrintString(GetWorld(), FString::Printf(TEXT("Movement Speed: %f"), Snake->GetActorTickInterval()), true, false); // ��� ������ �� �����, ���� �� ������� ���������� ����������,
			}																																			// ������� %������������������������� ��� ������ �����
			ASnakeBonusSpeed::Destroy();
		}

	}

}