// Fill out your copyright notice in the Description page of Project Settings.


#include "BlockSnake.h"
#include "SnakeBase.h"
#include "Kismet/GameplayStatics.h"

// Sets default values
ABlockSnake::ABlockSnake()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

}

// Called when the game starts or when spawned
void ABlockSnake::BeginPlay()
{
	Super::BeginPlay();
	
}

// Called every frame
void ABlockSnake::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

void ABlockSnake::Interact(AActor* Interactor, bool bIsHead)
{
	//���������� ������, ��������� �������
	auto  Snake = Cast<ASnakeBase>(Interactor);
	if (IsValid(Snake))
	{
		Snake->Destroy();
		UGameplayStatics::OpenLevel(GetWorld(), FName ("End_Game"), true);
	}
}