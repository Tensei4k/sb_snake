// Fill out your copyright notice in the Description page of Project Settings.


#include "SnakeBase.h"
#include "SnakeElementBase.h"
#include "Interactable.h"
#include "PlayerPawnBase.h"
#include "Kismet/GameplayStatics.h"
//#include "Kismet/KismetSystemLibrary.h"

// Sets default values
ASnakeBase::ASnakeBase()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;
	ElementSize = 100.f;
	MovementSpeed = 10.f;
	LastMoveDiraction = EMovementDiraction::DOWN;
	SnakeElementCounter = 1;
}

// Called when the game starts or when spawned
void ASnakeBase::BeginPlay()
{
	Super::BeginPlay();
	SetActorTickInterval(MovementSpeed);
	AddSnakeElement(5);
	
	
}

// Called every frame
void ASnakeBase::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
	Move();

}

void ASnakeBase::AddSnakeElement(int ElementsNum)
{
	
	for (int i = 0; i < ElementsNum; ++i)
	{
		//ASnakeElementBase->MeshComponent->SetActorHiddenInGame(true);
		FVector NewLocation(SnakeElements.Num() * ElementSize, 0, 0);
		FTransform NewTransform(NewLocation); 
		ASnakeElementBase* NewSnakeElem = GetWorld()->SpawnActor<ASnakeElementBase>(SnakeElementClass, NewTransform) ;
		NewSnakeElem->SnakeOwner = this;
		int32 ElemIndex = SnakeElements.Add(NewSnakeElem);
		NewSnakeElem->SetActorHiddenInGame(true);
		if (ElemIndex == 0)
		{
			NewSnakeElem->SetFirstElementType();
			//NewSnakeElem->MeshComponent->OnComponentBeginOverlap.AddDynamic(this, &ASnakeElementBase::HandleBeginOverlap);
		}
	}
	SnakeElementCounter += 1;
	//UKismetSystemLibrary::PrintString(GetWorld(), FString::Printf(TEXT("Counter: %i"), SnakeElementCounter), true, false);
	if (SnakeElementCounter > 6)
			{
				UGameplayStatics::OpenLevel(GetWorld(), FName("WInWIn"), true);
			}
}

void ASnakeBase::Move( )
{
	FVector MovementVector(ForceInitToZero);
	//APlayerPawnBase bMoveCorrection(false);
	switch (LastMoveDiraction)
	{
	case EMovementDiraction::UP:
		MovementVector.X += ElementSize;
		break;
	case EMovementDiraction::DOWN:
		MovementVector.X -= ElementSize;
		break;
	case EMovementDiraction::LEFT:
		MovementVector.Y += ElementSize;
		break;
	case EMovementDiraction::RIGHT:
		MovementVector.Y -= ElementSize;
		break;
	}

	SnakeElements[0]->ToggleCollision();

	for (int i = SnakeElements.Num() - 1; i > 0; i--)
	{
		auto CurrentElement = SnakeElements[i];
		auto PrevElement = SnakeElements[i - 1];
		PrevElement->SetActorHiddenInGame(false);
		FVector PrevLocation = PrevElement->GetActorLocation();
		CurrentElement->SetActorLocation(PrevLocation);
	}

	SnakeElements[0]->AddActorWorldOffset(MovementVector);
	SnakeElements[0]->ToggleCollision();

	APlayerPawnBase* Pawn = Cast<APlayerPawnBase>(UGameplayStatics::GetPlayerPawn(GetWorld(), 0));

	if (Pawn)
	{
		Pawn->IsChargedDirection = false;
	}
	
}

void ASnakeBase::SnakeElementOverlap(ASnakeElementBase* OverlappedElement, AActor* Other)
{
	if (IsValid(OverlappedElement))
	{
		int32 ElemIndex;
		SnakeElements.Find(OverlappedElement, ElemIndex);
		bool bIsFirst = ElemIndex == 0;
		IInteractable* InteractableInterface = Cast<IInteractable>(Other);
		if (InteractableInterface)
		{
			InteractableInterface->Interact(this, bIsFirst);
		}
	}
}

