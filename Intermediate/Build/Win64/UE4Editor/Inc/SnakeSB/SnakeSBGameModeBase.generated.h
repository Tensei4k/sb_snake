// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef SNAKESB_SnakeSBGameModeBase_generated_h
#error "SnakeSBGameModeBase.generated.h already included, missing '#pragma once' in SnakeSBGameModeBase.h"
#endif
#define SNAKESB_SnakeSBGameModeBase_generated_h

#define SnakeSB_Source_SnakeSB_SnakeSBGameModeBase_h_15_SPARSE_DATA
#define SnakeSB_Source_SnakeSB_SnakeSBGameModeBase_h_15_RPC_WRAPPERS
#define SnakeSB_Source_SnakeSB_SnakeSBGameModeBase_h_15_RPC_WRAPPERS_NO_PURE_DECLS
#define SnakeSB_Source_SnakeSB_SnakeSBGameModeBase_h_15_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesASnakeSBGameModeBase(); \
	friend struct Z_Construct_UClass_ASnakeSBGameModeBase_Statics; \
public: \
	DECLARE_CLASS(ASnakeSBGameModeBase, AGameModeBase, COMPILED_IN_FLAGS(0 | CLASS_Transient | CLASS_Config), CASTCLASS_None, TEXT("/Script/SnakeSB"), NO_API) \
	DECLARE_SERIALIZER(ASnakeSBGameModeBase)


#define SnakeSB_Source_SnakeSB_SnakeSBGameModeBase_h_15_INCLASS \
private: \
	static void StaticRegisterNativesASnakeSBGameModeBase(); \
	friend struct Z_Construct_UClass_ASnakeSBGameModeBase_Statics; \
public: \
	DECLARE_CLASS(ASnakeSBGameModeBase, AGameModeBase, COMPILED_IN_FLAGS(0 | CLASS_Transient | CLASS_Config), CASTCLASS_None, TEXT("/Script/SnakeSB"), NO_API) \
	DECLARE_SERIALIZER(ASnakeSBGameModeBase)


#define SnakeSB_Source_SnakeSB_SnakeSBGameModeBase_h_15_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API ASnakeSBGameModeBase(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(ASnakeSBGameModeBase) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, ASnakeSBGameModeBase); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(ASnakeSBGameModeBase); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API ASnakeSBGameModeBase(ASnakeSBGameModeBase&&); \
	NO_API ASnakeSBGameModeBase(const ASnakeSBGameModeBase&); \
public:


#define SnakeSB_Source_SnakeSB_SnakeSBGameModeBase_h_15_ENHANCED_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API ASnakeSBGameModeBase(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()) : Super(ObjectInitializer) { }; \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API ASnakeSBGameModeBase(ASnakeSBGameModeBase&&); \
	NO_API ASnakeSBGameModeBase(const ASnakeSBGameModeBase&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, ASnakeSBGameModeBase); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(ASnakeSBGameModeBase); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(ASnakeSBGameModeBase)


#define SnakeSB_Source_SnakeSB_SnakeSBGameModeBase_h_15_PRIVATE_PROPERTY_OFFSET
#define SnakeSB_Source_SnakeSB_SnakeSBGameModeBase_h_12_PROLOG
#define SnakeSB_Source_SnakeSB_SnakeSBGameModeBase_h_15_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	SnakeSB_Source_SnakeSB_SnakeSBGameModeBase_h_15_PRIVATE_PROPERTY_OFFSET \
	SnakeSB_Source_SnakeSB_SnakeSBGameModeBase_h_15_SPARSE_DATA \
	SnakeSB_Source_SnakeSB_SnakeSBGameModeBase_h_15_RPC_WRAPPERS \
	SnakeSB_Source_SnakeSB_SnakeSBGameModeBase_h_15_INCLASS \
	SnakeSB_Source_SnakeSB_SnakeSBGameModeBase_h_15_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define SnakeSB_Source_SnakeSB_SnakeSBGameModeBase_h_15_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	SnakeSB_Source_SnakeSB_SnakeSBGameModeBase_h_15_PRIVATE_PROPERTY_OFFSET \
	SnakeSB_Source_SnakeSB_SnakeSBGameModeBase_h_15_SPARSE_DATA \
	SnakeSB_Source_SnakeSB_SnakeSBGameModeBase_h_15_RPC_WRAPPERS_NO_PURE_DECLS \
	SnakeSB_Source_SnakeSB_SnakeSBGameModeBase_h_15_INCLASS_NO_PURE_DECLS \
	SnakeSB_Source_SnakeSB_SnakeSBGameModeBase_h_15_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> SNAKESB_API UClass* StaticClass<class ASnakeSBGameModeBase>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID SnakeSB_Source_SnakeSB_SnakeSBGameModeBase_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
